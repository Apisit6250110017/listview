import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // const MyApp({Key? key}) : super(key: key);

  final titles = [
    'bike',
    'boat',
    'bus',
    'car',
    'railway',
    'run',
    'subway',
    'transit',
    'walk'
  ];
  final images = [
    NetworkImage('https://i.pinimg.com/564x/b8/e7/64/b8e764226334534f279fb2cb9ae9d11a.jpg'),
    NetworkImage(
        'https://jordanw3m.files.wordpress.com/2011/09/boat.jpg'),
    NetworkImage(
        'https://i.pinimg.com/564x/ed/8d/a0/ed8da0258d72d3c9dec0408e62bbcebd.jpg'),
    NetworkImage(
        'https://i.pinimg.com/564x/35/53/79/35537956c39a039152f4a165e57d2016.jpg'),
    NetworkImage(
        'https://image.shutterstock.com/image-vector/blue-cartoon-train-600w-336777317.jpg'),
    NetworkImage('https://i.pinimg.com/564x/cf/d5/3d/cfd53d2dd7231bcec613f1058c22f590.jpg'),
    NetworkImage(
        'https://i.pinimg.com/564x/f3/5a/c5/f35ac5e585aa063803d1d9d87055feb7.jpg'),
    NetworkImage(
        'https://cssign.co.th/wp-content/uploads/TW03.jpg'),
    NetworkImage(
        'https://i.pinimg.com/736x/e1/17/c4/e117c42eecc43ee4b5966d2c135fe4e6.jpg'),
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Basic ListView',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Scaffold(
          appBar: AppBar(
            title: Text('ลิสต์วิว'),
          ),
          body: ListView.builder(
            itemCount: titles.length,
            itemBuilder: (BuildContext context, int index) {
              return Column(
                children: [
                  ListTile(
                    leading: CircleAvatar(
                      backgroundImage: images[index],
                    ),
                    title: Text(
                      '${titles[index]}',
                      style: TextStyle(fontSize: 18),
                    ),
                    subtitle: Text(
                      'There are many passengers in serveral vehicles',
                      style: TextStyle(
                        fontSize: 15,
                      ),
                    ),
                    trailing: Icon(
                      Icons.notifications_none,
                      size: 25,
                    ),
                    onTap: () {
                      AlertDialog alert = AlertDialog(
                        title: Text(
                            'Welcome'), // To display the title it is optional
                        content: Text(
                            'This is a ${titles[index]}'), // Message which will be pop up on the screen
                        // Action widget which will provide the user to acknowledge the choice
                        actions: [
                          ElevatedButton(
                            // FlatButton widget is used to make a text to work like a button

                            onPressed: () {
                              Navigator.of(context).pop();
                            }, // function used to perform after pressing the button
                            child: Text('CANCEL'),
                          ),
                          ElevatedButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text('ACCEPT'),
                          ),
                        ],
                      );
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return alert;
                        },
                      );
                    },
                  ),
                  Divider(
                    thickness: 0.8,
                  ),
                ],
              );
            },
          )),
    );
  }
}
